import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { OrderList } from './order-list/order-list.component';
import { ProductList } from './product-list/product-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Details } from './order-list/detail/detail.component';

const routes: Routes = [
  {path: 'order', component: OrderList},
  {path: 'product', component: ProductList},
  {path: 'order/:id', component: Details}
];

@NgModule({
  imports: [BrowserModule, FormsModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [OrderList,ProductList,Details]