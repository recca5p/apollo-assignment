import { Component, OnInit } from '@angular/core';
import { ProductService } from './product.service';
import { Router } from '@angular/router';
import { product } from './product.model';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductList implements OnInit{


  productDataList:any = [];
  constructor(public product: ProductService, private router: Router)
  {
  }
  ngOnInit(): void {
    this.loadData();
    
  }
  loadData() {
    this.product.getProduct()
    .subscribe(data => {
      this.productDataList = data;
    });
  }

  onSubmit (data: any) {
    this.product.createProduct(data).subscribe( reponse => {
      console.log(data);
      this.loadData();
    }
    )
  }

  deleteProduct (id:number) {
    this.product.deleteProduct(id)
    .subscribe(
      reponse => {
        this.loadData();
      }
    );
  }

  populateForm(selected: any) {
    this.product.formData = selected;
  }

  updateProduct(data: any) {
    this.product.updateProduct(data).subscribe(
      data => {
        this.loadData();
      }
    );
  }
  gotoAdd(): void {
    this.router.navigate(["/product/add"]);
  }

}