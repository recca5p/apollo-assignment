import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { product } from "./product.model";

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    constructor(private http: HttpClient) {}
    formData: product = new product();
    
    getProduct () {
        let url="https://localhost:44395/api/Product"
        return this.http.get(url);
    }
    getProductbyId (id:number) {
        let url="https://localhost:44395/api/Product"
        return this.http.get(url + "/"+id);
    }

    createProduct (data:any) {
        let url="https://localhost:44395/api/Product"
        return this.http.post(url, data);
    }

    updateProduct (data:any) {
        let url="https://localhost:44395/api/Product"
        
        return this.http.put(url, data);
    }

    deleteProduct (id:any) {
        let url="https://localhost:44395/api/Product";
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            body: {}
        } 
        return this.http.delete(url + "/" +id, options);
    }
}