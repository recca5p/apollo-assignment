import { Component, OnInit, ViewChild } from '@angular/core';
import { BillService } from './bill.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderList implements OnInit{

  productDataList:any = [];
  
  constructor(public bill: BillService, private router: Router)
  {
  }

  ngOnInit(): void {
    this.loadData();
  }
  loadData() {
    this.bill.getBill()
    .subscribe(data => {
      this.productDataList = data;
      console.log(data);
    });
  }

  onSubmit (data: any) {
    this.bill.createBill(data).subscribe( reponse => {
      console.log(data);
      this.loadData();
    }
    )
  }

  deleteBill (id:number) {
    this.bill.deleteBill(id)
    .subscribe(
      reponse => {
        this.loadData();
      }
    );
  }

  populateForm(selected: any) {
    this.bill.formData = Object.assign({}, selected);
  }

  updateBill(data: any) {
    this.bill.updateBill(data).subscribe(
      data => {
        this.loadData();
      }
    );
  }
  gotoDetail (id:any) {
    this.router.navigateByUrl("/order/"+id);
    this.bill.getUrlId(id);
  }

}