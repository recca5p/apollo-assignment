import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BillService } from "../bill.service";

@Injectable({
    providedIn: 'root'
})
export class BillProductService {
    constructor(private http: HttpClient, public bill: BillService) {}

    getBillByProduct (id:number) {
        let url="https://localhost:44395/api/BillByProduct"
        return this.http.get(url + "/"+id);
    }
}