import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/product-list/product.service';
import { BillService } from '../bill.service';
import { BillProductService } from './detail.service';
import { OrderList } from '../order-list.component';

@Component({
  selector: 'order-product-list',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class Details implements OnInit {
  id = 2;
  productDataList:any = [];
  constructor(public bill_pro: BillProductService, private router: Router, public bill: BillService)
  {
  }
  ngOnInit(): void {
    this.loadDataBillByProduct(this.bill.url_id);
  }
  loadDataBillByProduct(id:any) {
    this.bill_pro.getBillByProduct(id)
    .subscribe(data => {
      this.productDataList = data;
      console.log(data);
    });
  }
}