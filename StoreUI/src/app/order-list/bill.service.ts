import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { bill } from "./bill.model";

@Injectable({
    providedIn: 'root'
})
export class BillService {
    formData: bill = new bill();
    url_id: any;
    constructor(private http: HttpClient) {}

    getBill () {
        let url="https://localhost:44395/api/Bill";
        return this.http.get(url);
    }

    createBill (data:any) {
        let url="https://localhost:44395/api/Bill"
        return this.http.post(url, data);
    }

    updateBill (data:any) {
        let url="https://localhost:44395/api/Bill"
        return this.http.put(url, data);
    }

    deleteBill (id:any) {
        let url="https://localhost:44395/api/Bill"
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            }),
            body: {}
        } 
        return this.http.delete(url + "/" +id, options);
    }

    getUrlId (id:any) {
        this.url_id = id;
        return this.url_id;
    }
}