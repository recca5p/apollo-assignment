﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Store.Models;
using System.Data;
using System;
namespace Store.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    public class BillByProductController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public BillByProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //Get all product
        [HttpGet]
        [Route("{id}")]

        public JsonResult Getbyproduct(int id)
        {
            string query = @"select * from GetProductByBill ("+id+")";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult(table);
        }
    }
}
