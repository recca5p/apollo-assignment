﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Store.Models;
using System;
using System.Data;
using System.IO;

namespace Store.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]

    public class ProductController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;
        public ProductController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }
        //Get all product
        [HttpGet]
        public JsonResult Get()
        {
            string query = @"select * from GetProduct ()";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult(table);
        }
        //Get all product
        [HttpGet]
        [Route("{id}")]

        public JsonResult GetbyId(int id)
        {

            string query = @"select * from GetProductbyId ("+id+")";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult(table);
        }
        //Add a product product
        [HttpPost]
        public JsonResult Post(Product pro)
        {
            string query = @"CALL AddProduct(@productid,@productname,@productprice,@billid)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@productid", pro.productid);
                    myCommand.Parameters.AddWithValue("@productname", pro.productname);
                    myCommand.Parameters.AddWithValue("@productprice", pro.productprice);
                    myCommand.Parameters.AddWithValue("@billid", pro.billid);
                    try
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        myCon.Close();
                    }
                    catch (NpgsqlException ex)
                    {
                        Console.WriteLine(ex);
                    }


                }
            }
            return new JsonResult("Add successfully");
        }

        //Update a product
        [HttpPut]
        public JsonResult Update(Product pro)
        {
            string query = @"CALL UpdateProduct(@productid,@productname,@productprice,@billid)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@productid", pro.productid);
                    myCommand.Parameters.AddWithValue("@productname", pro.productname);
                    myCommand.Parameters.AddWithValue("@productprice", pro.productprice);
                    myCommand.Parameters.AddWithValue("@billid", pro.billid);
                    try
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        myCon.Close();
                    }
                    catch (Npgsql.NpgsqlException ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
            }
            return new JsonResult("Update successfully");
        }
        [Route("{id}")]
        //Delete a product
        [HttpDelete]
        public JsonResult Delete(Product pro, int id)
        {
            string query = @"CALL DeleteProduct(@productid)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@productid", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult("Delete successfully");
        }


    }
}
