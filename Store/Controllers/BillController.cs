﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Npgsql;
using Store.Models;
using System.Data;
using System;

namespace Store.Controllers
{
    [ApiController]
    [Route("api/[controller]/")]
    public class BillController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public BillController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //Get all product
        [HttpGet]
        public JsonResult Get()
        {
            string query = @"select * from GetBill ()";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult(table);
        }

        
        //Add a bill
        [HttpPost]  
        public JsonResult Post(Bill bill)
        {
            string query = @"CALL AddBill(@BillId,@TotalPrice)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@BillId", bill.billid);
                    myCommand.Parameters.AddWithValue("@TotalPrice", bill.totalprice);
                    try
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        myCon.Close();
                    }
                    catch (Npgsql.NpgsqlException ex)
                    {
                        Console.WriteLine(ex);
                    }


                }
            }
            return new JsonResult("Add successfully");
        }

        //Update a bill
        [HttpPut]
        public JsonResult Update(Bill bill)
        {
            string query = @"CALL UpdateBill(@BillId,@TotalPrice)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@BillId", bill.billid);
                    myCommand.Parameters.AddWithValue("@TotalPrice", bill.totalprice);
                    try
                    {
                        myReader = myCommand.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        myCon.Close();
                    }
                    catch (Npgsql.NpgsqlException ex)
                    {
                        Console.WriteLine(ex);
                    }

                }
            }
            return new JsonResult("Update successfully");
        }

        //Delete a product
        [HttpDelete]
        [Route("{id}")]
        public JsonResult Delete(Bill bill, int id)
        {
            string query = @"CALL DeleteBill(@BillId)";

            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("StoreAppCon");
            NpgsqlDataReader myReader;
            using (NpgsqlConnection myCon = new NpgsqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (NpgsqlCommand myCommand = new NpgsqlCommand(query, myCon))
                {
                    myCommand.Parameters.AddWithValue("@BillId", id);
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    myCon.Close();

                }
            }
            return new JsonResult("Delete successfully");
        }
    }
}
