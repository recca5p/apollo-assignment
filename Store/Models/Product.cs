﻿namespace Store.Models
{
    public class Product
    {
        public int productid { get; set; }
        public string productname { get; set; }
        public decimal productprice { get; set; }
        public int billid { get; set; }
    }
}
