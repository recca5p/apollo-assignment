﻿namespace Store.Models
{
    public class BillByProduct
    {
        public int productid { get; set; }
        public string productname { get; set; }
        public decimal productprice { get; set; }

    }
}
