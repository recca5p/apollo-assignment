﻿namespace Store.Models
{
    public class Bill
    {
        public int billid { get; set; }
        public decimal totalprice { get; set; }
    }
}
